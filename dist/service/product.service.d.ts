import { Repository } from 'typeorm';
import { Product } from '../entities/product.entity';
export declare class ProductService {
    private readonly productRepository;
    constructor(productRepository: Repository<Product>);
    findAll(): Promise<Product[]>;
    createProduct(productData: Partial<Product>): Promise<Product>;
    deleteProduct(productId: number): Promise<void>;
}
