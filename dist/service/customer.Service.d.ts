import { Repository } from 'typeorm';
import { Customer } from '../entities/customer.entity';
export declare class CustomerService {
    private customerRepository;
    constructor(customerRepository: Repository<Customer>);
    findAll(): Promise<Customer[]>;
    createCustomer(customerData: Partial<Customer>): Promise<Customer>;
    deleteCustomer(customerId: number): Promise<void>;
}
