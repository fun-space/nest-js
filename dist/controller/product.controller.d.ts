import { ProductService } from '../service/product.service';
import { Product } from '../entities/product.entity';
export declare class ProductController {
    private readonly productService;
    constructor(productService: ProductService);
    findAll(): Promise<Product[]>;
    createProduct(productData: Partial<Product>): Promise<Product>;
    deleteProduct(id: string): Promise<void>;
}
