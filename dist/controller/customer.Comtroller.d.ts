import { Customer } from '../entities/customer.entity';
import { CustomerService } from '../service/customer.Service';
export declare class CustomerController {
    private readonly customerService;
    constructor(customerService: CustomerService);
    findAll(): Promise<Customer[]>;
    createCustomer(customerData: Partial<Customer>): Promise<Customer>;
    deleteCustomer(id: string): Promise<void>;
}
