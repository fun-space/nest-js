import { BaseEntity } from 'typeorm';
export declare class Product extends BaseEntity {
    id: number;
    name: string;
    amount: number;
    price: number;
}
