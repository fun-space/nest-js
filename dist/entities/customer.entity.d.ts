import { BaseEntity } from 'typeorm';
export declare class Customer extends BaseEntity {
    id: number;
    name: string;
}
