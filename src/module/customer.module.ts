import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CustomerController } from '../controller/customer.Comtroller';
import { Customer } from '../entities/customer.entity';
import { CustomerService } from '../service/customer.Service';

@Module({
  imports: [TypeOrmModule.forFeature([Customer])],
  controllers: [CustomerController],
  providers: [CustomerService],
})
export class CustomerModule { }
