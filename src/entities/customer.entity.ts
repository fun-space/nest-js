import { Entity, Column, PrimaryGeneratedColumn, BaseEntity } from 'typeorm';

@Entity()
export class Customer extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  //本来想用实训课的数据库，但连接时就报错外键约束啥啥啥的。就新建个先用。
//   @Column()
//   Password: string;

//   @Column()
//   Location: string;

//   @Column()
//   Phonenumber: string;
//   @Column()
//   Sex: string;


}