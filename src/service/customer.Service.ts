import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Customer } from '../entities/customer.entity';

@Injectable()
export class CustomerService {
  constructor(
    @InjectRepository(Customer)
    private customerRepository: Repository<Customer>,
  ) {}

  //查询
  async findAll(): Promise<Customer[]> {
    return this.customerRepository.find();
  }

  /**
   * 新增顾客
   * @param productData 顾客数据
   * @returns 创建的顾客实体
   * async关键字用于表示该方法是异步的，可以使用await来等待返回的异步结果。
   */
  async createCustomer(customerData: Partial<Customer>): Promise<Customer> {
    const customer = this.customerRepository.create(customerData);
    return await this.customerRepository.save(customer);
  }

  /**
   * 删除顾客
   * @param productId 顾客ID
   * @returns 无返回值
   * async关键字用于表示该方法是异步的，可以使用await来等待返回的异步结果。
   */
  async deleteCustomer(customerId: number): Promise<void> {
    await this.customerRepository.delete(customerId);
  }
}