import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Product } from '../entities/product.entity';

@Injectable()
export class ProductService {
  constructor(
    @InjectRepository(Product)
    private readonly productRepository: Repository<Product>,
  ) {}

  //查询
  async findAll(): Promise<Product[]> {
    return this.productRepository.find();
  }

  /**
   * 创建产品
   * @param productData 产品数据
   * @returns 创建的产品实体
   * async关键字用于表示该方法是异步的，可以使用await来等待返回的异步结果。
   */
  async createProduct(productData: Partial<Product>): Promise<Product> {
    const product = this.productRepository.create(productData);
    return await this.productRepository.save(product);
  }

  /**
   * 删除产品
   * @param productId 产品ID
   * @returns 无返回值
   * async关键字用于表示该方法是异步的，可以使用await来等待返回的异步结果。
   */
  async deleteProduct(productId: number): Promise<void> {
    await this.productRepository.delete(productId);
  }
}