import { Controller, Get, Post, Body, Delete, Param } from '@nestjs/common';
import { ProductService } from '../service/product.service';
import { Product } from '../entities/product.entity';


@Controller('product')
export class ProductController {
  constructor(private readonly productService: ProductService) { }

  @Get('list')
  findAll(): Promise<Product[]> {
    return this.productService.findAll();
  }

  /**
   * 创建产品
   * @param productData 产品数据
   * @returns 创建的产品实体
   */
  @Post()
  async createProduct(@Body() productData: Partial<Product>): Promise<Product> {
    return await this.productService.createProduct(productData);
  }

  /**
   * 删除产品
   * @param id 产品ID
   * @returns 无返回值
   */
  @Delete('del/:id')
  async deleteProduct(@Param('id') id: string): Promise<void> {
    await this.productService.deleteProduct(Number(id));
  }
}
