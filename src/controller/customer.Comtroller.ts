import { Controller, Get, Post, Body, Delete, Param } from '@nestjs/common';
import { Customer } from '../entities/customer.entity';
import { CustomerService } from '../service/customer.Service';


@Controller('customer')
export class CustomerController {
  constructor(private readonly customerService: CustomerService) { }

  @Get('list')
  findAll(): Promise<Customer[]> {
    return this.customerService.findAll();
  }
  
  /**
   * 创建产品
   * @param productData 产品数据
   * @returns 创建的产品实体
   */
  @Post()
  async createCustomer(@Body() customerData: Partial<Customer>): Promise<Customer> {
    return await this.customerService.createCustomer(customerData);
  }

  /**
   * 删除顾客
   * @param id 顾客ID
   * @returns 无返回值
   */
  @Delete('del/:id')
  async deleteCustomer(@Param('id') id: string): Promise<void> {
    await this.customerService.deleteCustomer(Number(id));
  }
}
